# Presentable

[![Build Status](https://api.travis-ci.org/rowofpixels/presentable.svg?branch=master)](https://travis-ci.org/rowofpixels/presentable)
&nbsp;
[![Latest Stable Version](https://poser.pugx.org/rowofpixels/presentable/version.svg)](https://packagist.org/packages/rowofpixels/presentable)
&nbsp;
[![License](https://poser.pugx.org/rowofpixels/presentable/license.svg)](https://packagist.org/packages/rowofpixels/presentable)
&nbsp;
[![Coverage Status](https://img.shields.io/coveralls/rowofpixels/presentable.svg)](https://coveralls.io/r/rowofpixels/presentable)

### Summary
Presentable is a package for making your objects presentable. It uses the presenter pattern to allow moving presentation logic out of other objects.

### Installation
Install this package through [Composer](https://getcomposer.org/)
```json
{
    "require": {
        "rowofpixels/presentable": "0.1.*"
    }
}
```

### Usage
Set up a basic presenter class that extends `Rowofpixels\Presentable\Presenter`. You can access the entity you are presenting with `$this->entity`.


Use `Rowofpixels\Presentable\PresentableTrait` in an object and set the presenter class name to the present you created.

```php
<?php

namespace Acme;

use Rowofpixels\Presentable\Presenter;

class PersonPresenter extends Presenter
{
    public function fullName()
    {
        return $this->entity->firstName . ' ' . $this->entity->lastName;
    }
}

...

use Rowofpixels\Presentable\PresentableTrait;

class Person
{
    use PresentableTrait;
    protected $presenter = 'Acme\PersonPresenter';
}

...

$person = new \Acme\Person();
$person->firstName = 'Michael';
$person->lastName = 'Bluth';
$person->present()->fullName();
$person->present()->fullName; // Attribute syntax also works
```
