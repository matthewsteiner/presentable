<?php
namespace Rowofpixels\Presentable;

interface PresentableInterface
{
    public function presenter();
}
