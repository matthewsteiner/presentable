<?php
namespace Rowofpixels\Presentable;

trait PresentableTrait
{
    public function present()
    {
        $className = $this->presenter;

        return new $className($this);
    }
}
