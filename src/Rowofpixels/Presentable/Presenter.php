<?php
namespace Rowofpixels\Presentable;

use BadFunctionCallException;
use InvalidArgumentException;

class Presenter
{
    protected $entity;

    public function __construct($entity = null)
    {
        if (!$entity) {
            throw new InvalidArgumentException;
        }

        $this->entity = $entity;
    }

    public function __get($name)
    {
        if (method_exists($this, $name)) {
            return $this->$name();
        }

        throw new BadFunctionCallException;
    }
}
