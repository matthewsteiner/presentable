<?php
namespace Rowofpixels\Presentable;

use Illuminate\Database\Eloquent\Model as Eloquent;

class EntityPresenter extends Presenter
{
    public function name()
    {
        return 'value';
    }
}

class Entity
{
    use PresentableTrait;

    protected $presenter = 'Rowofpixels\Presentable\EntityPresenter';
}

class PresentableTraitTest extends TestCase
{
    public function testEntityIsInstantiable()
    {
        new Entity();
    }

    public function testEntityCanPresent()
    {
        $entity = new Entity();

        $this->assertInstanceOf('Rowofpixels\Presentable\Presenter', $entity->present());
    }

    public function testEntityPresenterHasAttributeMethod()
    {
        $entity = new Entity();

        $this->assertEquals('value', $entity->present()->name());
    }
}
