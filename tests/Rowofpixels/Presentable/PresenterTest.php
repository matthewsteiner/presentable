<?php
namespace Rowofpixels\Presentable;

use stdClass as Object;

class Model {}
class PersonPresenter extends Presenter
{
    public function fullName()
    {
        return $this->entity->firstName . ' ' . $this->entity->lastName;
    }
}

class PresenterTest extends TestCase
{
    public function testPresenterCanBeInstantiated()
    {
        $model = new Model;

        new Presenter($model);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testPresenterRequiresObject()
    {
        new Presenter();
    }

    public function testPresenterMethodCanUseEntity()
    {
        $entity = new Object();
        $entity->firstName = 'Michael';
        $entity->lastName = 'Bluth';
        $presenter = new PersonPresenter($entity);

        $this->assertEquals('Michael Bluth', $presenter->fullName());
    }

    public function testPresenterCanUseAttributeSyntax()
    {
        $entity = new Object();
        $entity->firstName = 'Michael';
        $entity->lastName = 'Bluth';
        $presenter = new PersonPresenter($entity);

        $this->assertEquals('Michael Bluth', $presenter->fullName);
    }

    /**
     * @expectedException BadFunctionCallException
     */
    public function testPresenterThrowsExceptionForUndefinedAttribute()
    {
        $entity = new Object();
        $presenter = new PersonPresenter($entity);

        $presenter->nonsense;
    }
}
