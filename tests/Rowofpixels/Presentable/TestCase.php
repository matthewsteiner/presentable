<?php
namespace Rowofpixels\Presentable;

use PHPUnit_Framework_TestCase;

class TestCase extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->setTimezone();
    }

    private function setTimezone()
    {
        date_default_timezone_set('America/Denver');
    }
}
